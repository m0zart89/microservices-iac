module "yc-kubernetes" {
  source     = "github.com/terraform-yc-modules/terraform-yc-kubernetes.git"
  network_id = module.yc-vpc.vpc_id

  master_locations = [
    {
      zone      = "${module.yc-vpc.private_subnets["10.10.0.0/24"].zone}"
      subnet_id = "${module.yc-vpc.private_subnets["10.10.0.0/24"].subnet_id}"
    }
  ]

  node_groups = {
    "yc-k8s-ng-01" = {
      description = "Kubernetes nodes group 01"
      fixed_scale = {
        size = 4
      }
      node_labels = {
        role      = "worker-01"
        service   = "kubernetes"
        terraform = "true"
      }
    }
  }
}
