terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone      = "ru-central1-a"
  cloud_id  = "b1gcfjb0l4t8iqig7rc2"
  folder_id = "b1gtvjl7b0ci815pp0ln"
}
